"""An AWS Python Pulumi program"""

import pulumi
import pulumi_aws as aws
from pulumi import get_stack, get_project

stack = get_stack()
project = get_project()
app = "MyApp"
env = "%s-DEV" % (app.title())

if stack == "app":

    role = aws.iam.Role(
        "%s%sRole" % (project.title(), app.title()),
        assume_role_policy="""{
            "Version": "2012-10-17",
            "Statement": [
                {
                "Action": "sts:AssumeRole",
                "Principal": {
                    "Service": "ec2.amazonaws.com"
                },
                "Effect": "Allow",
                "Sid": ""
                }
            ]
        }""",
        path="/"
    )

    policy = aws.iam.RolePolicy(
        "%s%sPolicy" % (project.title(), app.title()),
        name="WebServerRole",
        policy="""{
            "Version": "2012-10-17",
            "Statement": [
                {
                "Action": [
                    "iam:*"
                ],
                "Effect": "Allow",
                "Resource": "*"
                }
            ]
        }""",
        role=role.name
    )

    instance_profile = aws.iam.InstanceProfile(
        "%s%sInstanceProfile" % (project.title(), app.title()),
        name="%s%sInstanceProfile" % (project.title(), app.title()),
        role=role.name,
        path="/")

    ebapp = aws.elasticbeanstalk.Application(
        "%s%sBeanstalkApplication" % (project.title(), app.title()),
        name="%s" % (app),
        description="%s Beanstalk application for %s project" % (app.title(),
                                                                project.title())

    )
    if stack == "env":

ebenv = aws.elasticbeanstalk.Environment(
    "%s%sBeanstalkEnvironnement" % (project.title(), app.title()),
    application=app,
    name=env,
    solution_stack_name="64bit Amazon Linux 2 v3.1.3 running PHP 7.4",
    settings=[
        aws.elasticbeanstalk.EnvironmentSettingArgs(
            namespace="aws:autoscaling:launchconfiguration",
            name="IamInstanceProfile",
            value="%s%sInstanceProfile" % (project.title(), app.title())
        ),
        aws.elasticbeanstalk.EnvironmentSettingArgs(
            namespace="aws:ec2:instances",
            name="InstanceTypes",
            value="t3.micro"
        ),
        aws.elasticbeanstalk.EnvironmentSettingArgs(
            namespace="aws:elasticbeanstalk:environment",
            name="EnvironmentType",
            value="LoadBalanced"
        ),
        aws.elasticbeanstalk.EnvironmentSettingArgs(
            namespace="aws:autoscaling:asg",
            name="MinSize",
            value="1"
        ),
        aws.elasticbeanstalk.EnvironmentSettingArgs(
            namespace="aws:autoscaling:asg",
            name="MaxSize",
            value="1"
        )
    ])
